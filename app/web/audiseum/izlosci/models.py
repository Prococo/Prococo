from django.db import models
from datetime import timedelta
from django.conf import settings


class GrupaIzlozaka(models.Model):
    idgrupa = models.AutoField(primary_key=True)
    idnadgrupa = models.IntegerField()
    nazivgrupa = models.CharField(max_length=255)

    class Meta:
        managed = True
        db_table = 'grupeizlozaka'


class Izlozak(models.Model):
    idizlozak = models.AutoField(primary_key=True)
    nazivizlozak = models.CharField(max_length=255)
    opis = models.TextField()
    idzvucnizapis = models.ForeignKey('ZvucniZapis', on_delete=models.CASCADE, db_column='idzvucnizapis')  # ?? on_delete ??
    qrkod = models.CharField(max_length=255)
    idslika = models.ForeignKey('Slika', on_delete=models.CASCADE, db_column='idslika') # ?? on_delete ??
    grupa = models.ManyToManyField(GrupaIzlozaka)

    class Meta:
        managed = True
        db_table = 'izlosci'


class Slika(models.Model):
    idslika = models.AutoField(primary_key=True)
    slika = models.FileField(upload_to='slike/')

    class Meta:
        managed = True
        db_table = 'slike'


class ZvucniZapis(models.Model):
    idzvucnizapis = models.AutoField(primary_key=True)
    zvucnizapis = models.FileField(upload_to='audio/')
    brojkorisnika = models.BigIntegerField(default=0)
    brojreprodukcija = models.BigIntegerField(default=0)

    class Meta:
        managed = True
        db_table = 'zvucnizapisi'


class PromotivniMaterijal(models.Model):
    idmaterijal = models.AutoField(primary_key=True)
    materijal = models.FileField(upload_to='promo/')

    class Meta:
        managed = True
        db_table = 'promotivnimaterijali'


class PosjetStranice(models.Model):
    idposjet = models.AutoField(primary_key=True)
    idstranica = models.ForeignKey('StatistikaPosjecenosti', on_delete=models.CASCADE, db_column='idstranica')
    vrijemeotvaranja = models.DateTimeField()
    vrijemezatvaranja = models.DateTimeField()

    class Meta:
        managed = True
        db_table = 'posjetestranica'


class StranicaIzlozak(models.Model):
    idstranica = models.ForeignKey('StatistikaPosjecenosti', on_delete=models.CASCADE, db_column='idstranica')
    idizlozak = models.ForeignKey('Izlozak', on_delete=models.CASCADE, db_column='idizlozak')

    class Meta:
        managed = True
        db_table = 'stranicaizlozak'


class StatistikaPosjecenosti(models.Model):
    idstranica = models.BigIntegerField(primary_key=True)
    brojprikaza = models.BigIntegerField(default=0)
    vrijemezadrzavanja = models.DurationField(default=timedelta())

    class Meta:
        managed = True
        db_table = 'statistikaposjecenosti'


class StatistikaReprodukcije(models.Model):
    korisnik = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True)
    idzvucnizapis = models.ForeignKey('ZvucniZapis', on_delete=models.CASCADE, db_column='idzvucnizapis')

    class Meta:
        managed = True
        db_table = 'statistikareprodukcija'


class AdministratorskaOvlast(models.Model):
    idovlast = models.AutoField(primary_key=True)
    opis = models.TextField()

    class Meta:
        managed = True
        db_table = 'administratorskeovlasti'


class Ovlasti(models.Model):
    class Meta:
        permissions = (("can_view", "Smije pogledati"),)


class Aplikacija(models.Model):
    aplikacijaid = models.TextField(primary_key=True)
    korisnik = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,)

    class Meta:
        managed = True
        db_table = 'aplikacija'
