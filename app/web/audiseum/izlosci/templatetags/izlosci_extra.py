from django import template
from django.contrib.auth.models import Group
from izlosci.models import Izlozak, GrupaIzlozaka

register = template.Library()


@register.filter(name='has_group')
def has_group(user, group_name):
    group = Group.objects.get(name=group_name)
    return True if group in user.groups.all() else False

# {{ user|has_group:group_name }}


@register.filter(name='dio_grupe')
def izlozak_group(izlozak, group_id):

    sve_grupe = GrupaIzlozaka.objects.all()
    nadgrupe_i_grupa = set(GrupaIzlozaka.objects.filter(idgrupa=group_id))
    loop = True
    while loop:
        loop = False
        for trenutna_grupa in sve_grupe:
            nadgrupa = GrupaIzlozaka.objects.get(idgrupa=trenutna_grupa.idnadgrupa)
            if nadgrupa in nadgrupe_i_grupa and trenutna_grupa not in nadgrupe_i_grupa:
                nadgrupe_i_grupa.add(trenutna_grupa)
                loop = True

    for grupa in nadgrupe_i_grupa:
        if izlozak in grupa.izlozak_set.all():
            return True
    return False

# {{ izlozak|dio_grupe:group_id }}
