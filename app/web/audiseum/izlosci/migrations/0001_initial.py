from django.db import migrations


def add_groups(apps, schema_editor):

    Group = apps.get_model("auth", "Group")

    registrirani_korisnici, created = Group.objects.get_or_create(name='registrirani korisnici')
    administratori, created = Group.objects.get_or_create(name='administratori')
    vlasnik, created = Group.objects.get_or_create(name='vlasnik')


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.RunPython(add_groups)
    ]