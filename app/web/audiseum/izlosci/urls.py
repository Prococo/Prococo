from django.urls import path
import izlosci.views
import statistika.views


app_name = 'izlosci'

urlpatterns = [
    path('', izlosci.views.izlosci, name='izlosci'),
    path('grupa/<grupa_id>/', izlosci.views.izlosci_grupa, name='izlosci_grupa'),
    path('izlozak/<izlozak_id>/', izlosci.views.izlozak, name='izlozak'),
    path('promo-materijali/', izlosci.views.promo_materijali, name='promo_materijali'),
    path('registracija/', izlosci.views.register, name='registracija'),
    path('activate/<uidb64>/<token>/', izlosci.views.activate, name='activate'),
    path('prijava/', izlosci.views.prijava, name='login'),
    path('odjava/', izlosci.views.odjava, name='odjava'),
    path('uredi-osobne-podatke/', izlosci.views.uredi_osobne_podatke, name='uredi_osobne_podatke'),
    path('promijeni-lozinku', izlosci.views.promijeni_lozinku, name='promijeni_lozinku'),
    path('../statistika', statistika.views.statistika, name='statistika'),
    path('dodaj-admina/', izlosci.views.dodaj_admina, name='dodaj_admina'),
    path('ovlast/', izlosci.views.dodaj_admin_ovlast, name='ovlast'),
    path('dodaj-izlozak/', izlosci.views.dodaj_izlozak, name='dodaj_izlozak'),
    path('uredi-izlozak/<izlozak_id>/', izlosci.views.uredi_izlozak, name='uredi_izlozak'),
    path('obrisi-izlozak/<izlozak_id>/', izlosci.views.obrisi_izlozak, name='obrisi_izlozak'),
    path('dodaj-grupu/', izlosci.views.dodaj_grupu, name='dodaj_grupu'),
    path('dodaj-promo-materijal', izlosci.views.dodaj_promo_materijal, name='dodaj_promo_materijal'),
    path('reprodukcija/<izlozak_id>', izlosci.views.reproduciraj, name='reproduciraj'),

]
