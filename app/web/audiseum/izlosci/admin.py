from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(Izlozak)
admin.site.register(GrupaIzlozaka)
admin.site.register(ZvucniZapis)
admin.site.register(PromotivniMaterijal)
admin.site.register(PosjetStranice)
admin.site.register(StatistikaPosjecenosti)
admin.site.register(StatistikaReprodukcije)
admin.site.register(AdministratorskaOvlast)
admin.site.register(Slika)
admin.site.register(StranicaIzlozak)
admin.site.register(Aplikacija)