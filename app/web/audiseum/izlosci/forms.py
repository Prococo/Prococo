from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, UserChangeForm, PasswordChangeForm
from django.contrib.auth.models import User
from .models import AdministratorskaOvlast, Izlozak, GrupaIzlozaka
import magic
from tinytag import TinyTag
import os
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.conf import settings


class RegistrationForm(UserCreationForm):
    username = forms.CharField(required=True, max_length=255, label="Korisničko ime",
                               widget=forms.TextInput(
                                   attrs={'placeholder': 'Upišite korisničko ime', 'class': 'form-control',
                                          'id': 'idUsernameRegister'}))
    first_name = forms.CharField(required=True, max_length=255, label="Ime",
                                 widget=forms.TextInput(
                                     attrs={'placeholder': 'Upišite ime', 'class': 'form-control'}))
    last_name = forms.CharField(required=True, max_length=255, label="Prezime",
                                widget=forms.TextInput(
                                    attrs={'placeholder': 'Upišite prezime', 'class': 'form-control'}))
    email = forms.EmailField(required=True, max_length=255, label="E-mail",
                             widget=forms.EmailInput(
                                 attrs={'placeholder': 'Upišite e-mail adresu', 'class': 'form-control'}))
    password1 = forms.CharField(required=True, max_length=255, label="Lozinka",
                                widget=forms.PasswordInput(
                                    attrs={'placeholder': 'Upišite lozinku', 'class': 'form-control'}))
    password2 = forms.CharField(required=True, max_length=255,
                                widget=forms.PasswordInput(
                                    attrs={'placeholder': 'Potvrdite lozinku', 'class': 'form-control'}))

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password1', 'password2']

    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.email = self.cleaned_data['email']

        if commit:
            user.save()

        return user


class LoginForm(AuthenticationForm):
    username = forms.CharField(required=True, max_length=255, label="Korisničko ime",
                               widget=forms.TextInput(
                                   attrs={'placeholder': 'Upišite korisničko ime', 'class': 'form-control',
                                          'id': 'idUsernameLogin'}))
    password = forms.CharField(required=True, max_length=255, label="Password",
                               widget=forms.PasswordInput(
                                   attrs={'placeholder': 'Upišite lozinku', 'class': 'form-control'}))


class UrediOsobnePodatkeForm(UserChangeForm):
    username = forms.CharField(required=True, max_length=255, label="Korisničko ime",
                               widget=forms.TextInput(
                                   attrs={'placeholder': 'Upišite korisničko ime', 'class': 'form-control',
                                          'id': 'idUsernameEdit'}))
    first_name = forms.CharField(required=True, max_length=255, label="Ime",
                                 widget=forms.TextInput(
                                     attrs={'placeholder': 'Upišite ime', 'class': 'form-control'}))
    last_name = forms.CharField(required=True, max_length=255, label="Prezime",
                                widget=forms.TextInput(
                                    attrs={'placeholder': 'Upišite prezime', 'class': 'form-control'}))
    email = forms.EmailField(required=True, max_length=255, label="E-mail",
                             widget=forms.EmailInput(
                                 attrs={'placeholder': 'Upišite e-mail adresu', 'class': 'form-control'}))

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password']


class PromijeniLozinkuForm(PasswordChangeForm):
    old_password = forms.CharField(required=True, max_length=255,
                                   widget=forms.PasswordInput(
                                       attrs={'placeholder': 'Upišite staru lozinku', 'class': 'form-control'}))
    new_password1 = forms.CharField(required=True, max_length=255,
                                    widget=forms.PasswordInput(
                                        attrs={'placeholder': 'Upišite novu lozinku', 'class': 'form-control'}))
    new_password2 = forms.CharField(required=True, max_length=255,
                                    widget=forms.PasswordInput(
                                        attrs={'placeholder': 'Potvrdite novu lozinku', 'class': 'form-control'}))

    class Meta:
        fields = ['old_password', 'new_password1', 'new_password2']


class AddAdminForm(forms.Form):
    username = forms.CharField(required=True, max_length=255, label="Korisničko ime",
                               widget=forms.TextInput(attrs={'placeholder': 'Upišite korisničko ime administratora',
                                                             'class': 'form-control', 'id': 'idUsernameAddAdmin'}))


class AdminOvlastForm(forms.ModelForm):
    opis = forms.CharField(required=True, label="Opis ovlasti", widget=forms.Textarea(
        attrs={'placeholder': 'Upišite opis administratorske ovlasti', 'class': 'form-control',
               'id': 'idAdminOvlastOpis'}))

    class Meta:
        model = AdministratorskaOvlast
        fields = ['opis']


def get_grupe():
    grupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
    sveGrupe = []
    for grupa in grupe:
        sveGrupe.append(tuple([grupa.nazivgrupa, grupa.nazivgrupa]))

    sveGrupe.append(tuple(["Bez grupe", "Bez grupe"]))

    return sveGrupe


class DodajIzlozakForm(forms.Form):

    class CheckboxSelectMultiple(forms.MultipleChoiceField):
        choices = get_grupe()
        attrs = {'id': 'grupeAdd'}
    #     # class Media:
    #     #     js = ('group-dropdown.js')

    def __init__(self, *args, **kwargs):
        super(DodajIzlozakForm, self).__init__(*args, **kwargs)
        self.fields['Grupe'] = forms.MultipleChoiceField(choices=get_grupe())

    nazivizlozak = forms.CharField(required=True, max_length=255, label="Naziv", widget=forms.TextInput(
        attrs={'placeholder': 'Upišite naziv izloška', 'class': 'form-control'}))

    opis = forms.CharField(required=True, label="Opis", widget=forms.Textarea(
        attrs={'placeholder': 'Upišite opis izloška', 'class': 'form-control', 'id': 'idOpisIzlozak'}))

    # grupe = forms.MultipleChoiceField(required=False, label="Grupe",
    #                                      widget=forms.CheckboxSelectMultiple(choices=get_grupe(),
    #                                                                          attrs={'id': 'grupeAdd'}))

    # grupa = forms.ModelMultipleChoiceField(queryset=GrupaIzlozaka.objects.all())

    slika = forms.FileField(widget=forms.FileInput(attrs={'class': 'form-control-file'}))

    zvucnizapis = forms.FileField(widget=forms.FileInput(attrs={'class': 'form-control-file'}))

    def clean_slika(self):
        file = self.cleaned_data["slika"]
        filetype = magic.from_buffer(file.read())
        acceptable_types = ["PNG", "JPG", "JPEG"]
        if not any(i in filetype for i in acceptable_types):
            raise forms.ValidationError("Datoteka mora biti PNG ili JPG.")
        return file

    def clean_zvucnizapis(self):
        file = self.cleaned_data["zvucnizapis"]
        filetype = magic.from_buffer(file.read())

        path = default_storage.save('tmp/temporary.mp3', ContentFile(file.read()))
        tmp_zvuk = os.path.join(settings.MEDIA_ROOT, path)
        tag = TinyTag.get(tmp_zvuk)

        acceptable_types = ["MP3", "mp3", "mpeg3", "MPEG"]
        if not any(i in filetype for i in acceptable_types):
            raise forms.ValidationError("Datoteka mora biti MP3.")
        if tag.duration > 180:
            raise forms.ValidationError("Zvuk je dulji od 3 minute")
        return file


class UrediIzlozakForm(forms.Form):

    class CheckboxSelectMultiple(forms.MultipleChoiceField):
        choices = get_grupe()
        attrs = {'id': 'grupeAdd'}
    #     # class Media:
    #     #     js = ('group-dropdown.js')

    def __init__(self, *args, **kwargs):
        super(UrediIzlozakForm, self).__init__(*args, **kwargs)
        self.fields['Grupe'] = forms.MultipleChoiceField(choices=get_grupe(), required=False)

    nazivizlozak = forms.CharField(required=True, max_length=255, label="Naziv", widget=forms.TextInput(
        attrs={'placeholder': 'Upišite naziv izloška', 'class': 'form-control'}))

    opis = forms.CharField(required=True, label="Opis", widget=forms.Textarea(
        attrs={'placeholder': 'Upišite opis izloška', 'class': 'form-control', 'id': 'idOpisIzlozak'}))

    # grupa = forms.ModelMultipleChoiceField(queryset=GrupaIzlozaka.objects.all())

    slika = forms.FileField(required=False, widget=forms.FileInput(attrs={'class': 'form-control-file'}))

    zvucnizapis = forms.FileField(required=False, widget=forms.FileInput(attrs={'class': 'form-control-file'}))

    def clean_slika(self):
        file = self.cleaned_data["slika"]
        if file is None:
            return None
        filetype = magic.from_buffer(file.read())
        acceptable_types = ["PNG", "JPG", "JPEG"]
        if not any(i in filetype for i in acceptable_types):
            raise forms.ValidationError("File is not PNG or JPG.")
        return file

    def clean_zvucnizapis(self):
        file = self.cleaned_data["zvucnizapis"]
        if file is None:
            return None
        filetype = magic.from_buffer(file.read())

        path = default_storage.save('tmp/temporary.mp3', ContentFile(file.read()))
        tmp_zvuk = os.path.join(settings.MEDIA_ROOT, path)
        tag = TinyTag.get(tmp_zvuk)

        acceptable_types = ["MP3", "mp3", "mpeg3", "MPEG"]
        if not any(i in filetype for i in acceptable_types):
            raise forms.ValidationError("File is not MP3.")
        if tag.duration > 180:
            raise forms.ValidationError("Zvuk je dulji od 3 minute")
        return file


class ObrisiIzlozakForm(forms.ModelForm):
    class Meta:
        model = Izlozak
        fields = []


def get_nadgrupe():
    grupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
    nadgrupe = []
    for grupa in grupe:
        if grupa.idgrupa == grupa.idnadgrupa:
            nadgrupe.append(tuple([grupa.nazivgrupa, grupa.nazivgrupa]))

    nadgrupe.append(tuple(["Nema", "Nema"]))

    return nadgrupe


class AddGroupForm(forms.Form):

    class CheckboxSelectMultiple(forms.MultipleChoiceField):
        # choices=get_nadgrupe()
        # attrs={'id': 'nadgrupeAdd'}
        class Media:
            js = ('group-dropdown.js')

    def __init__(self, *args, **kwargs):
        super(AddGroupForm, self).__init__(*args, **kwargs)
        self.fields['Nadgrupe'] = forms.ChoiceField(choices=get_nadgrupe())

    nazivgrupa = forms.CharField(required=True, max_length=255, label="Naziv",
                                 widget=forms.TextInput(
                                     attrs={'placeholder': 'Upišite naziv grupe', 'class': 'form-control',
                                            'id': 'nameAddGroup'}))

    # nadgrupe = forms.MultipleChoiceField(required=False, label="Grupe",
    #                                      widget=forms.CheckboxSelectMultiple(choices=get_nadgrupe(),
    #                                                                          attrs={'id': 'nadgrupeAdd'}))


class PromoMaterijalForm(forms.Form):
    materijal = forms.FileField(widget=forms.FileInput(attrs={'class': 'form-control-file'}))
