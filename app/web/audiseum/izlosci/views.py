from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import *
from django.contrib.auth.models import User, Group
from .forms import (
    RegistrationForm, LoginForm,
    UrediOsobnePodatkeForm, PromijeniLozinkuForm,
    AddAdminForm, AdminOvlastForm,
    DodajIzlozakForm, UrediIzlozakForm, ObrisiIzlozakForm,
    AddGroupForm, PromoMaterijalForm)
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.template.loader import render_to_string
from izlosci.tokens import account_activation_token
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
import sendgrid
from sendgrid.helpers.mail import *
import datetime
from django.utils import timezone


# HELPER
def stat_time(request):
    try:
        # provjeri je li zadnja stranica bila otvorena, ako si ti prva, nemoj mijenjati vrijeme provedeno
        id_zadnja_stranica = request.session['id_zadnja_stranica']
        id_zadnji_posjet = request.session['id_zadnji_posjet']
        posjet = PosjetStranice.objects.get(idposjet=id_zadnji_posjet)
        stat = StatistikaPosjecenosti.objects.get(idstranica=id_zadnja_stranica)
        posjet.vrijeme_zatvaranja = timezone.now()
        vrijeme_od_proslog_posjeta = (timezone.now() - posjet.vrijemeotvaranja)
        if vrijeme_od_proslog_posjeta > timedelta(minutes=10):
            vrijeme_od_proslog_posjeta = timedelta(minutes=10)
        stat.vrijemezadrzavanja += vrijeme_od_proslog_posjeta
        stat.save()
        posjet.save()

    except (PosjetStranice.DoesNotExist, KeyError):
        return
    return


# HELPER
def stat_clicks_time(request, id_trenutna_stranica):
    stat_time(request)
    # dodaj statistiku posjecenosti ako ona ne postoji, ako postoji
    try:
        stranica_stat = StatistikaPosjecenosti.objects.get(idstranica=id_trenutna_stranica)

    except StatistikaPosjecenosti.DoesNotExist:
        stranica_stat = StatistikaPosjecenosti(
            idstranica=id_trenutna_stranica, brojprikaza=0, vrijemezadrzavanja=datetime.timedelta(0))
        stranica_stat.save()

    try:
        trenutni_izlozak = Izlozak.objects.get(idizlozak=id_trenutna_stranica)

        try:
            StranicaIzlozak.objects.get(idizlozak=trenutni_izlozak,idstranica=stranica_stat)
        except StranicaIzlozak.DoesNotExist:
            izstr = StranicaIzlozak(idstranica=stranica_stat, idizlozak=trenutni_izlozak)
            izstr.save()

    except Izlozak.DoesNotExist:  # ako izlozak s tim id ne postoji, onda je to pocetna stranica i nista se ne dogadja
        ...

    posjet = PosjetStranice(
        idstranica=stranica_stat,
        vrijemeotvaranja=timezone.now().strftime('%Y-%m-%d %H:%M:%S'),
        vrijemezatvaranja=(timezone.now() + datetime.timedelta(minutes=1)).strftime('%Y-%m-%d %H:%M:%S'))

    stranica_stat.brojprikaza += 1
    stranica_stat.save()
    posjet.save()

    request.session['id_zadnja_stranica'] = id_trenutna_stranica
    request.session['id_zadnji_posjet'] = PosjetStranice.objects.latest('idposjet').idposjet


def android_povezi(request):
    try:  # dio za povezivanje android aplikacije
        unique_id = request.session["unique_id"]
        nova_aplikacija = Aplikacija(aplikacijaid=unique_id, korisnik=request.user,)
        nova_aplikacija.save()
        return True
    except:
        return False


def index(request):
    return redirect('/izlosci')


def nadgrupe(sveGrupe):
    grupeSPodgrupama = set()
    for grupa in sveGrupe:
        if grupa.idgrupa == grupa.idnadgrupa:
            for podgrupa in sveGrupe:
                if podgrupa.idnadgrupa == grupa.idgrupa and podgrupa.idgrupa != podgrupa.idnadgrupa:
                    grupeSPodgrupama.add(grupa.idgrupa)
                    break
    return grupeSPodgrupama


def izlosci(request):
    # statistika za broj posjeta i provedeno vrijeme
    stat_clicks_time(request, 0)

    sviIzlosci = Izlozak.objects.all().order_by('nazivizlozak')
    sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
    grupeSPodgrupama = nadgrupe(sveGrupe)

    context = {'sviIzlosci': sviIzlosci,
               'sveGrupe': sveGrupe,
               'grupeSPodgrupama': grupeSPodgrupama,
               'register_form': RegistrationForm(),
               'login_form': LoginForm(),
               'add_admin_form': AddAdminForm(),
               'admin_ovlast_form': AdminOvlastForm(),
               'dodaj_izlozak_form': DodajIzlozakForm(),
               'add_group_form': AddGroupForm(),
               'promo_materijal_form': PromoMaterijalForm()}
    return render(request, 'izlosci/izlosci.html', context)


def izlosci_grupa(request, grupa_id):
    # statistika za broj posjeta i provedeno vrijeme
    stat_clicks_time(request, 0)

    sviIzlosci = Izlozak.objects.all().order_by('nazivizlozak')
    grupa = GrupaIzlozaka.objects.get(pk=grupa_id)
    sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
    grupeSPodgrupama = nadgrupe(sveGrupe)

    context = {'sviIzlosci': sviIzlosci,
               'sveGrupe': sveGrupe,
               'grupeSPodgrupama': grupeSPodgrupama,
               'grupa': grupa,
               'register_form': RegistrationForm(),
               'login_form': LoginForm(),
               'add_admin_form': AddAdminForm(),
               'admin_ovlast_form': AdminOvlastForm(),
               'dodaj_izlozak_form': DodajIzlozakForm(),
               'add_group_form': AddGroupForm(),
               'promo_materijal_form': PromoMaterijalForm()}
    return render(request, 'izlosci/izlosci.html', context)


def izlozak(request, izlozak_id):
    # statistika za broj posjeta i provedeno vrijeme
    stat_clicks_time(request, izlozak_id)

    izlozak = Izlozak.objects.get(pk=izlozak_id)
    sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
    grupeSPodgrupama = nadgrupe(sveGrupe)

    context = {'izlozak': izlozak,
               'sveGrupe': sveGrupe,
               'grupeSPodgrupama': grupeSPodgrupama,
               'register_form': RegistrationForm(),
               'login_form': LoginForm(),
               'add_admin_form': AddAdminForm(),
               'admin_ovlast_form': AdminOvlastForm(),
               'dodaj_izlozak_form': DodajIzlozakForm(),
               'add_group_form': AddGroupForm(),
               'promo_materijal_form': PromoMaterijalForm()}
    return render(request, 'izlosci/izlozak.html', context)


def promo_materijali(request):
    promoMaterijali = PromotivniMaterijal.objects.all()
    sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
    grupeSPodgrupama = nadgrupe(sveGrupe)

    context = {'promoMaterijali': promoMaterijali,
               'sveGrupe': sveGrupe,
               'grupeSPodgrupama': grupeSPodgrupama,
               'add_admin_form': AddAdminForm(),
               'admin_ovlast_form': AdminOvlastForm(),
               'dodaj_izlozak_form': DodajIzlozakForm(),
               'add_group_form': AddGroupForm(),
               'promo_materijal_form': PromoMaterijalForm()}
    return render(request, 'izlosci/promotivniMaterijali.html', context)


def register(request):
    stat_time(request)  # spremanje provedenog vremena na prethodnoj stranici
    if request.method == 'POST':
        register_form = RegistrationForm(request.POST)
        if register_form.is_valid():
            user = register_form.save(commit=False)
            user.is_active = False
            user.save()  # TODO: ne smije user.save() kad registracija prode a nije prihvacena
            current_site = get_current_site(request)
            message = render_to_string('izlosci/aktivacija_racuna_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': force_text(urlsafe_base64_encode(force_bytes(user.pk))),
                'token': account_activation_token.make_token(user),
            })

            sg = sendgrid.SendGridAPIClient(
                apikey='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')
            from_email = Email(email="no-reply@audiseum.com", name="Aktivacija")
            to_email = Email(user.email)
            subject = "Aktivirajte svoj Audiseum račun"
            content = Content("text/html", message)
            mail = Mail(from_email, subject, to_email, content)
            sg.client.mail.send.post(request_body=mail.get())

            sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
            grupeSPodgrupama = nadgrupe(sveGrupe)
            poruka = 'Molimo Vas aktivirajte svoj korisnički račun klikom na link koji Vam je poslan na e-mail adresu.'
            context = {'sveGrupe': sveGrupe,
                       'grupeSPodgrupama': grupeSPodgrupama,
                       'register_form': RegistrationForm(),
                       'login_form': LoginForm(),
                       'poruka': poruka,
                       'add_admin_form': AddAdminForm(),
                       'admin_ovlast_form': AdminOvlastForm(),
                       'dodaj_izlozak_form': DodajIzlozakForm(),
                       'add_group_form': AddGroupForm(),
                       'promo_materijal_form': PromoMaterijalForm()}
            return render(request, 'izlosci/poruka.html', context)
        else:
            sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
            grupeSPodgrupama = nadgrupe(sveGrupe)
            poruka = 'Registracija nije uspjela! Molimo Vas pokušajte ponovno.'
            context = {'sveGrupe': sveGrupe,
                       'grupeSPodgrupama': grupeSPodgrupama,
                       'register_form': RegistrationForm(),
                       'login_form': LoginForm(),
                       'poruka': poruka,
                       'add_admin_form': AddAdminForm(),
                       'admin_ovlast_form': AdminOvlastForm(),
                       'dodaj_izlozak_form': DodajIzlozakForm(),
                       'add_group_form': AddGroupForm(),
                       'promo_materijal_form': PromoMaterijalForm()}
            return render(request, 'izlosci/poruka.html', context)


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(bytes(uidb64, 'utf-8')))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        registrirani_korisnici = Group.objects.get(name='registrirani korisnici')
        registrirani_korisnici.user_set.add(user)
        user.is_active = True
        user.save()
        login(request, user)

        if android_povezi(request):  # povezivanje s androidom, ako uspije onda ispisuje ovu poruku
            return HttpResponse('Povezivanje uspješno! Sada se možete vratiti u aplikaciju')

        sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
        grupeSPodgrupama = nadgrupe(sveGrupe)
        poruka = 'Aktivacija korisničkog računa uspješna!'
        context = {'sveGrupe': sveGrupe,
                   'grupeSPodgrupama': grupeSPodgrupama,
                   'poruka': poruka,
                   'add_admin_form': AddAdminForm(),
                   'admin_ovlast_form': AdminOvlastForm(),
                   'dodaj_izlozak_form': DodajIzlozakForm(),
                   'add_group_form': AddGroupForm(),
                   'promo_materijal_form': PromoMaterijalForm()}
        return render(request, 'izlosci/poruka.html', context)
    else:
        sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
        grupeSPodgrupama = nadgrupe(sveGrupe)
        poruka = 'Aktivacijski link nevažeći!'
        context = {'sveGrupe': sveGrupe,
                   'grupeSPodgrupama': grupeSPodgrupama,
                   'poruka': poruka,
                   'add_admin_form': AddAdminForm(),
                   'admin_ovlast_form': AdminOvlastForm(),
                   'dodaj_izlozak_form': DodajIzlozakForm(),
                   'add_group_form': AddGroupForm(),
                   'promo_materijal_form': PromoMaterijalForm()}
        return render(request, 'izlosci/poruka.html', context)


def account_activation_sent(request):
    stat_time(request)  # spremanje provedenog vremena na prethodnoj stranici
    sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
    grupeSPodgrupama = nadgrupe(sveGrupe)
    poruka = 'Molimo Vas aktivirajte svoj korisnički račun klikom na link koji Vam je poslan na e-mail adresu.'
    context = {'sveGrupe': sveGrupe,
                    'grupeSPodgrupama': grupeSPodgrupama,
                    'register_form': RegistrationForm(),
                    'login_form': LoginForm(),
                    'poruka': poruka,
                    'add_admin_form': AddAdminForm(),
                    'admin_ovlast_form': AdminOvlastForm(),
                    'dodaj_izlozak_form': DodajIzlozakForm(),
                    'add_group_form': AddGroupForm(),
                    'promo_materijal_form': PromoMaterijalForm()}
    return render(request, 'izlosci/poruka.html', context)


def prijava(request):
    stat_time(request)  # spremanje provedenog vremena na prethodnoj stranici
    if request.method == 'POST':
        login_form = LoginForm(data=request.POST)
        print(login_form.errors, type(login_form.errors))
        if login_form.is_valid():

            user = authenticate(request, username=login_form.cleaned_data['username'],
                                password=login_form.cleaned_data['password'])
            if user is not None:
                login(request, user)

                if android_povezi(request):  # povezivanje s androidom, ako uspije onda ispisuje ovu poruku
                    return HttpResponse('Povezivanje uspješno! Sada se možete vratiti u aplikaciju')
                # return render(request, 'izlosci/izlosci.html')
                return redirect('/izlosci')

    sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
    grupeSPodgrupama = nadgrupe(sveGrupe)
    poruka = 'Prijava neuspješna! Molimo Vas pokušajte ponovno.'
    context = {'sveGrupe': sveGrupe,
               'grupeSPodgrupama': grupeSPodgrupama,
               'register_form': RegistrationForm(),
               'login_form': LoginForm(),
               'poruka': poruka,
               'add_admin_form': AddAdminForm(),
               'admin_ovlast_form': AdminOvlastForm(),
               'dodaj_izlozak_form': DodajIzlozakForm(),
               'add_group_form': AddGroupForm(),
               'promo_materijal_form': PromoMaterijalForm()}
    return render(request, 'izlosci/poruka.html', context)


def odjava(request):
    stat_time(request)  # spremanje provedenog vremena na prethodnoj stranici
    logout(request)
    return redirect('/izlosci')


def uredi_osobne_podatke(request):
    stat_time(request)  # spremanje provedenog vremena na prethodnoj stranici
    if request.method == 'POST':
        uredi_osobne_podatke_form = UrediOsobnePodatkeForm(request.POST, instance=request.user)
        if uredi_osobne_podatke_form.is_valid():
            uredi_osobne_podatke_form.save()
            sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
            grupeSPodgrupama = nadgrupe(sveGrupe)
            poruka = 'Uspješno ste izmijenili svoje osobne podatke!'
            context = {'sveGrupe': sveGrupe,
                       'grupeSPodgrupama': grupeSPodgrupama,
                       'poruka': poruka,
                       'add_admin_form': AddAdminForm(),
                       'admin_ovlast_form': AdminOvlastForm(),
                       'dodaj_izlozak_form': DodajIzlozakForm(),
                       'add_group_form': AddGroupForm(),
                       'promo_materijal_form': PromoMaterijalForm()}
            return render(request, 'izlosci/poruka.html', context)
        else:
            sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
            grupeSPodgrupama = nadgrupe(sveGrupe)
            poruka = 'Izmjena osobnih podataka neuspješna! Molimo Vas pokušajte ponovno.'
            context = {'sveGrupe': sveGrupe,
                       'grupeSPodgrupama': grupeSPodgrupama,
                       'poruka': poruka,
                       'add_admin_form': AddAdminForm(),
                       'admin_ovlast_form': AdminOvlastForm(),
                       'dodaj_izlozak_form': DodajIzlozakForm(),
                       'add_group_form': AddGroupForm(),
                       'promo_materijal_form': PromoMaterijalForm()}
            return render(request, 'izlosci/poruka.html', context)
    else:
        uredi_osobne_podatke_form = UrediOsobnePodatkeForm(instance=request.user)
        sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
        grupeSPodgrupama = nadgrupe(sveGrupe)
        context = {'sveGrupe': sveGrupe,
                   'grupeSPodgrupama': grupeSPodgrupama,
                   'uredi_osobne_podatke_form': uredi_osobne_podatke_form,
                   'add_admin_form': AddAdminForm(),
                   'admin_ovlast_form': AdminOvlastForm(),
                   'dodaj_izlozak_form': DodajIzlozakForm(),
                   'add_group_form': AddGroupForm(),
                   'promo_materijal_form': PromoMaterijalForm()}
        return render(request, 'izlosci/urediOsobnePodatke.html', context)


def promijeni_lozinku(request):
    stat_time(request)  # spremanje provedenog vremena na prethodnoj stranici
    if request.method == 'POST':
        promijeni_lozinku_form = PromijeniLozinkuForm(data=request.POST, user=request.user)
        if promijeni_lozinku_form.is_valid():
            promijeni_lozinku_form.save()
            update_session_auth_hash(request, promijeni_lozinku_form.user)
            sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
            grupeSPodgrupama = nadgrupe(sveGrupe)
            poruka = 'Uspješno ste promijenili svoju lozinku!'
            context = {'sveGrupe': sveGrupe,
                       'grupeSPodgrupama': grupeSPodgrupama,
                       'poruka': poruka,
                       'add_admin_form': AddAdminForm(),
                       'admin_ovlast_form': AdminOvlastForm(),
                       'dodaj_izlozak_form': DodajIzlozakForm(),
                       'add_group_form': AddGroupForm(),
                       'promo_materijal_form': PromoMaterijalForm()}
            return render(request, 'izlosci/poruka.html', context)
        else:
            sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
            grupeSPodgrupama = nadgrupe(sveGrupe)
            poruka = 'Promjena lozinke neuspješna! Molimo Vas pokušajte ponovno.'
            context = {'sveGrupe': sveGrupe,
                       'grupeSPodgrupama': grupeSPodgrupama,
                       'poruka': poruka,
                       'add_admin_form': AddAdminForm(),
                       'admin_ovlast_form': AdminOvlastForm(),
                       'dodaj_izlozak_form': DodajIzlozakForm(),
                       'add_group_form': AddGroupForm(),
                       'promo_materijal_form': PromoMaterijalForm()}
            return render(request, 'izlosci/poruka.html', context)
    else:
        promijeni_lozinku_form = PromijeniLozinkuForm(user=request.user)
        sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
        grupeSPodgrupama = nadgrupe(sveGrupe)
        context = {'sveGrupe': sveGrupe,
                   'grupeSPodgrupama': grupeSPodgrupama,
                   'promijeni_lozinku_form': promijeni_lozinku_form,
                   'add_admin_form': AddAdminForm(),
                   'admin_ovlast_form': AdminOvlastForm(),
                   'dodaj_izlozak_form': DodajIzlozakForm(),
                   'add_group_form': AddGroupForm(),
                   'promo_materijal_form': PromoMaterijalForm()}
        return render(request, 'izlosci/promijeniLozinku.html', context)


def dodaj_admina(request):
    if request.method == 'POST':
        add_admin_form = AddAdminForm(request.POST)
        print(add_admin_form.errors, type(add_admin_form.errors))

        if add_admin_form.is_valid():
            username = add_admin_form.cleaned_data['username']
            user = User.objects.get(username=username)
            all_admins = User.objects.filter(groups=2)

            if user is not None:
                administratori = Group.objects.get(name='administratori')

                if all_admins.count() < 5:
                    administratori.user_set.add(user)
                    # user.groups.add(administratori)
                    sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
                    grupeSPodgrupama = nadgrupe(sveGrupe)
                    poruka = 'Uspješno ste dodali administratora!'
                    context = {'sveGrupe': sveGrupe,
                               'grupeSPodgrupama': grupeSPodgrupama,
                               'poruka': poruka,
                               'add_admin_form': AddAdminForm(),
                               'admin_ovlast_form': AdminOvlastForm(),
                               'dodaj_izlozak_form': DodajIzlozakForm(),
                               'add_group_form': AddGroupForm(),
                               'promo_materijal_form': PromoMaterijalForm()}
                    return render(request, 'izlosci/poruka.html', context)

    sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
    grupeSPodgrupama = nadgrupe(sveGrupe)
    poruka = 'Dodavanje administratora nije uspjelo!'
    context = {'sveGrupe': sveGrupe,
               'grupeSPodgrupama': grupeSPodgrupama,
               'poruka': poruka,
               'add_admin_form': AddAdminForm(),
               'admin_ovlast_form': AdminOvlastForm(),
               'dodaj_izlozak_form': DodajIzlozakForm(),
               'add_group_form': AddGroupForm(),
               'promo_materijal_form': PromoMaterijalForm()}
    return render(request, 'izlosci/poruka.html', context)


def dodaj_admin_ovlast(request):
    if request.method == 'POST':
        admin_ovlast_form = AdminOvlastForm(request.POST)
        if admin_ovlast_form.is_valid():
            ovlast = admin_ovlast_form.save(commit=False)
            ovlast.opis = admin_ovlast_form.cleaned_data['opis']
            ovlast.save()
            sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
            grupeSPodgrupama = nadgrupe(sveGrupe)
            poruka = 'Uspješno ste dodali administratorsku ovlast!'
            context = {'sveGrupe': sveGrupe,
                       'grupeSPodgrupama': grupeSPodgrupama,
                       'poruka': poruka,
                       'add_admin_form': AddAdminForm(),
                       'admin_ovlast_form': AdminOvlastForm(),
                       'dodaj_izlozak_form': DodajIzlozakForm(),
                       'add_group_form': AddGroupForm(),
                       'promo_materijal_form': PromoMaterijalForm()}
            return render(request, 'izlosci/poruka.html', context)

    sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
    grupeSPodgrupama = nadgrupe(sveGrupe)
    poruka = 'Dodavanje administratorske ovlasti nije uspjelo!'
    context = {'sveGrupe': sveGrupe,
               'grupeSPodgrupama': grupeSPodgrupama,
               'poruka': poruka,
               'add_admin_form': AddAdminForm(),
               'admin_ovlast_form': AdminOvlastForm(),
               'dodaj_izlozak_form': DodajIzlozakForm(),
               'add_group_form': AddGroupForm(),
               'promo_materijal_form': PromoMaterijalForm()}
    return render(request, 'izlosci/poruka.html', context)


def dodaj_izlozak(request):
    stat_time(request)  # spremanje provedenog vremena na prethodnoj stranici
    if request.method == 'POST':
        dodaj_izlozak_form = DodajIzlozakForm(request.POST, request.FILES)
        print(dodaj_izlozak_form.errors, type(dodaj_izlozak_form.errors))
        if dodaj_izlozak_form.is_valid():
            slika = Slika(slika=dodaj_izlozak_form.cleaned_data['slika'])
            slika.save()

            zvucnizapis = ZvucniZapis(
                zvucnizapis=dodaj_izlozak_form.cleaned_data['zvucnizapis'], brojkorisnika=0, brojreprodukcija=0)
            zvucnizapis.save()

            grupe = dodaj_izlozak_form.cleaned_data['Grupe']
            print(str(grupe))

            novi_izlozak = Izlozak(
                nazivizlozak=dodaj_izlozak_form.cleaned_data['nazivizlozak'],
                opis=dodaj_izlozak_form.cleaned_data['opis'], qrkod='qrkod', idzvucnizapis=zvucnizapis, idslika=slika,)
            novi_izlozak.save()

            for grup in grupe:
                if grup == "Bez grupe":
                    break
                grupa = GrupaIzlozaka.objects.get(nazivgrupa=grup)
                novi_izlozak.grupa.add(grupa)

            stranica_stat = StatistikaPosjecenosti(
                idstranica=novi_izlozak.idizlozak, brojprikaza=0, vrijemezadrzavanja=datetime.timedelta(0))
            stranica_stat.save()

            izstr = StranicaIzlozak(idstranica=stranica_stat, idizlozak=novi_izlozak)
            izstr.save()
            #DODAVANJE IZLOSKAGRUPE

            sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
            grupeSPodgrupama = nadgrupe(sveGrupe)
            poruka = 'Uspješno ste dodali novi izložak: ' + novi_izlozak.nazivizlozak
            context = {'sveGrupe': sveGrupe,
                       'grupeSPodgrupama': grupeSPodgrupama,
                       'poruka': poruka,
                       'add_admin_form': AddAdminForm(),
                       'admin_ovlast_form': AdminOvlastForm(),
                       'dodaj_izlozak_form': DodajIzlozakForm(),
                       'add_group_form': AddGroupForm(),
                       'promo_materijal_form': PromoMaterijalForm()}
            return render(request, 'izlosci/poruka.html', context)
        else:
            sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
            grupeSPodgrupama = nadgrupe(sveGrupe)
            poruka = 'Dodavanje izloška nije uspjelo!'
            context = {'sveGrupe': sveGrupe,
                       'grupeSPodgrupama': grupeSPodgrupama,
                       'poruka': poruka,
                       'add_admin_form': AddAdminForm(),
                       'admin_ovlast_form': AdminOvlastForm(),
                       'dodaj_izlozak_form': DodajIzlozakForm(),
                       'add_group_form': AddGroupForm(),
                       'promo_materijal_form': PromoMaterijalForm()}
            return render(request, 'izlosci/poruka.html', context)


def uredi_izlozak(request, izlozak_id):
    stat_time(request)  # spremanje provedenog vremena na prethodnoj stranici
    if request.method == 'POST':

        uredi_izlozak_form = UrediIzlozakForm(request.POST, request.FILES)
        izlozak = Izlozak.objects.get(pk=izlozak_id)

        stareGrupe = izlozak.grupa.all()
        print(str(stareGrupe))

        if uredi_izlozak_form.is_valid():
            nova_slika = uredi_izlozak_form.cleaned_data['slika']
            if nova_slika:
                slika_obj =izlozak.idslika
                slika_obj.slika = nova_slika
                slika_obj.save()

            novi_zvucni_zapis = uredi_izlozak_form.cleaned_data['zvucnizapis']
            if novi_zvucni_zapis:
                zvucnizapis_obj = izlozak.idzvucnizapis
                zvucnizapis_obj.zvucnizapis = novi_zvucni_zapis
                zvucnizapis_obj.save()

            izlozak.nazivizlozak = uredi_izlozak_form.cleaned_data['nazivizlozak']
            izlozak.opis = uredi_izlozak_form.cleaned_data['opis']

            izlozak.save(update_fields=['nazivizlozak', 'opis'])

            grupe = uredi_izlozak_form.cleaned_data['Grupe']
            # print(str(grupe))
            if len(grupe) != 0: #ako je korisnik izabrao neku novu grupu

                for stara in stareGrupe:
                        izlozak.grupa.remove(stara)

                #izlozak.grupa_set.clear()

                for grup in grupe:
                    if grup == "Bez grupe":
                        break
                    grupa = GrupaIzlozaka.objects.get(nazivgrupa=grup)
                    izlozak.grupa.add(grupa)

            sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
            grupeSPodgrupama = nadgrupe(sveGrupe)

            poruka = 'Uspješno ste izmijenili podatke izloška: ' + izlozak.nazivizlozak
            context = {'sveGrupe': sveGrupe,
                       'grupeSPodgrupama': grupeSPodgrupama,
                       'poruka': poruka,
                       'add_admin_form': AddAdminForm(),
                       'admin_ovlast_form': AdminOvlastForm(),
                       'dodaj_izlozak_form': DodajIzlozakForm(),
                       'add_group_form': AddGroupForm(),
                       'promo_materijal_form': PromoMaterijalForm()}
            return render(request, 'izlosci/poruka.html', context)
        else:
            sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
            grupeSPodgrupama = nadgrupe(sveGrupe)
            poruka = 'Izmjena podataka o izlošku nije uspjela!'
            context = {'sveGrupe': sveGrupe,
                       'grupeSPodgrupama': grupeSPodgrupama,
                       'poruka': poruka,
                       'add_admin_form': AddAdminForm(),
                       'admin_ovlast_form': AdminOvlastForm(),
                       'dodaj_izlozak_form': DodajIzlozakForm(),
                       'add_group_form': AddGroupForm(),
                       'promo_materijal_form': PromoMaterijalForm()}
            return render(request, 'izlosci/poruka.html', context)

    else:
        izlozak = Izlozak.objects.get(pk=izlozak_id)
        uredi_izlozak_form = UrediIzlozakForm(initial={'nazivizlozak': izlozak.nazivizlozak, 'opis': izlozak.opis})
        sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
        grupeSPodgrupama = nadgrupe(sveGrupe)
        context = {'uredi_izlozak_form': uredi_izlozak_form,
                   'izlozak': izlozak,
                   'sveGrupe': sveGrupe,
                   'grupeSPodgrupama': grupeSPodgrupama,
                   'add_admin_form': AddAdminForm(),
                   'admin_ovlast_form': AdminOvlastForm(),
                   'dodaj_izlozak_form': DodajIzlozakForm(),
                   'add_group_form': AddGroupForm(),
                   'promo_materijal_form': PromoMaterijalForm()}
        return render(request, 'izlosci/urediIzlozak.html', context)


def obrisi_izlozak(request, izlozak_id):
    if request.method == 'POST':
        obrisi_izlozak_form = ObrisiIzlozakForm(request.POST)
        if obrisi_izlozak_form.is_valid():
            izlozak = Izlozak.objects.get(pk=izlozak_id)
            ime_izlozak = izlozak.nazivizlozak
            statposj = StranicaIzlozak.objects.get(idizlozak=izlozak).idstranica

            izlozak.delete()
            statposj.delete()

            sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
            grupeSPodgrupama = nadgrupe(sveGrupe)
            poruka = 'Uspješno ste obrisali izložak ' + str(ime_izlozak) + '!'
            context = {'sveGrupe': sveGrupe,
                       'grupeSPodgrupama': grupeSPodgrupama,
                       'poruka': poruka,
                       'add_admin_form': AddAdminForm(),
                       'admin_ovlast_form': AdminOvlastForm(),
                       'dodaj_izlozak_form': DodajIzlozakForm(),
                       'add_group_form': AddGroupForm(),
                       'promo_materijal_form': PromoMaterijalForm()}
            return render(request, 'izlosci/poruka.html', context)

        sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
        grupeSPodgrupama = nadgrupe(sveGrupe)
        poruka = 'Brisanje izloška nije uspjelo!'
        context = {'sveGrupe': sveGrupe,
                   'grupeSPodgrupama': grupeSPodgrupama,
                   'poruka': poruka,
                   'add_admin_form': AddAdminForm(),
                   'admin_ovlast_form': AdminOvlastForm(),
                   'dodaj_izlozak_form': DodajIzlozakForm(),
                   'add_group_form': AddGroupForm(),
                   'promo_materijal_form': PromoMaterijalForm()}
        return render(request, 'izlosci/poruka.html', context)


def dodaj_grupu(request):
    if request.method == 'POST':
        add_group_form = AddGroupForm(request.POST)
        print(add_group_form.errors, type(add_group_form.errors))

        if add_group_form.is_valid():
            # grupa = add_group_form.save(commit=False)
            naziv = add_group_form.cleaned_data['nazivgrupa']
            nadgr = add_group_form.cleaned_data['Nadgrupe']

            print(naziv + " " + str(nadgr))

            sve = GrupaIzlozaka.objects.all()

            # ovo se moglo ljepše, ali ja ne znam, htjela sam izbjeci da mi uzme id koji je
            # već u tablici, pa tražim najvećeg dosad
            maxId = 0
            for gr in sve:
                if gr.idgrupa > maxId:
                    maxId = gr.idgrupa

            grupa = GrupaIzlozaka()
            grupa.nazivgrupa = naziv
            grupa.idgrupa = maxId + 1

            if nadgr != "Nema":
                nadgrupa = GrupaIzlozaka.objects.get(nazivgrupa=nadgr)
                grupa.idnadgrupa = nadgrupa.idgrupa
            else:
                grupa.idnadgrupa = grupa.idgrupa
            # grupa.idnadgrupa = nadgrupa.idgrupa
            # grupa.save()

            grupa.save()

            sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
            grupeSPodgrupama = nadgrupe(sveGrupe)
            poruka = 'Uspješno ste dodali novu grupu izložaka: ' + naziv
            context = {'sveGrupe': sveGrupe,
                       'grupeSPodgrupama': grupeSPodgrupama,
                       'poruka': poruka,
                       'add_admin_form': AddAdminForm(),
                       'admin_ovlast_form': AdminOvlastForm(),
                       'dodaj_izlozak_form': DodajIzlozakForm(),
                       'add_group_form': AddGroupForm(),
                       'promo_materijal_form': PromoMaterijalForm()}
            return render(request, 'izlosci/poruka.html', context)

        sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
        grupeSPodgrupama = nadgrupe(sveGrupe)
        poruka = 'Dodavanje grupe izložaka nije uspjelo!'
        context = {'sveGrupe': sveGrupe,
                   'grupeSPodgrupama': grupeSPodgrupama,
                   'poruka': poruka,
                   'add_admin_form': AddAdminForm(),
                   'admin_ovlast_form': AdminOvlastForm(),
                   'dodaj_izlozak_form': DodajIzlozakForm(),
                   'add_group_form': AddGroupForm(),
                   'promo_materijal_form': PromoMaterijalForm()}
        return render(request, 'izlosci/poruka.html', context)


def dodaj_promo_materijal(request):
    stat_time(request)  # spremanje provedenog vremena na prethodnoj stranici
    if request.method == 'POST':
        promo_materijal_form = PromoMaterijalForm(request.POST, request.FILES)
        if promo_materijal_form.is_valid():
            materijal = PromotivniMaterijal(materijal=promo_materijal_form.cleaned_data['materijal'])
            materijal.save()
            sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
            grupeSPodgrupama = nadgrupe(sveGrupe)
            poruka = 'Uspješno ste dodali novi promotivni materijal!'
            context = {'sveGrupe': sveGrupe,
                       'grupeSPodgrupama': grupeSPodgrupama,
                       'poruka': poruka,
                       'add_admin_form': AddAdminForm(),
                       'admin_ovlast_form': AdminOvlastForm(),
                       'dodaj_izlozak_form': DodajIzlozakForm(),
                       'add_group_form': AddGroupForm(),
                       'promo_materijal_form': PromoMaterijalForm()}
            return render(request, 'izlosci/poruka.html', context)
        else:
            sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
            grupeSPodgrupama = nadgrupe(sveGrupe)
            poruka = 'Dodavanje promotivnog materijala nije uspjelo!'
            context = {'sveGrupe': sveGrupe,
                       'grupeSPodgrupama': grupeSPodgrupama,
                       'poruka': poruka,
                       'add_admin_form': AddAdminForm(),
                       'admin_ovlast_form': AdminOvlastForm(),
                       'dodaj_izlozak_form': DodajIzlozakForm(),
                       'add_group_form': AddGroupForm(),
                       'promo_materijal_form': PromoMaterijalForm()}
            return render(request, 'izlosci/poruka.html', context)


def reproduciraj(request, izlozak_id):
    obj_zvucnizapis = ZvucniZapis.objects.get(izlozak=izlozak_id)
    try:
        obj_korisnik = request.user
        playback = StatistikaReprodukcije(korisnik=obj_korisnik, idzvucnizapis=obj_zvucnizapis)
        playback.save()
    except (AttributeError, ValueError):
        ...

    obj_zvucnizapis.brojreprodukcija += 1
    obj_zvucnizapis.save()

    izlozak = Izlozak.objects.get(pk=izlozak_id)

    return redirect("http://35.189.113.190:8000/" + str(izlozak.idzvucnizapis.zvucnizapis))


# TODO: forma i view za izmjenit izlozak
# TODO: reset password i ponovno slanje maila za registraciju
# TODO: osposobit dodavanje grupe u dodaj izlozak i uredi izlozak
# TODO: osposobit uredi podatke
# TODO: osposobit uredi izlozak
# TODO: promo materijali osposobit
# TODO: obrisat grupu
# TODO: Lijepo da se prikazuje
# TODO: grupa da se moze dodat najvecu razinu i tako to
# TODO: Limiti na slike i mp3 i duljinu mp3
# TODO: ljepše formatirat vrijeme zadržavanja
# TODO: statistika reprodukcija
# TODO: Ljepše formatirati vrijeme zadržavanja
# TODO: statistika provedenog vremena bilježi ponekad previše
# TODO: reprodukcija
# TODO: nadgrupe da se filtrira kad se stisne
