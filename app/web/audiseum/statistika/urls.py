from django.urls import path
from . import views

app_name = 'statistika'

urlpatterns = [
    path('', views.statistika, name='statistika'),
    path('<param>', views.statistika_sort, name='statistika_sort')
]