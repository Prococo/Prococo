from django.apps import AppConfig


class StatistikaConfig(AppConfig):
    name = 'statistika'
