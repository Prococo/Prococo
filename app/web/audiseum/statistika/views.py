from django.shortcuts import render
from izlosci.models import (
    StatistikaPosjecenosti, Izlozak,
    StranicaIzlozak, GrupaIzlozaka,
    StatistikaReprodukcije, ZvucniZapis)
from izlosci.forms import (
    UrediOsobnePodatkeForm, PromijeniLozinkuForm,
    AddAdminForm, AdminOvlastForm,
    DodajIzlozakForm, AddGroupForm,
    PromoMaterijalForm)
from django.contrib.auth.models import User
from operator import itemgetter, attrgetter
from django.contrib.sessions.models import Session
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse


@login_required
def statistika(request):

    if not request.user.groups.filter(name__in=['administratori', 'vlasnik']).exists():
        return HttpResponse("Zabranjeno")

    strIzlozak = StranicaIzlozak.objects.all()
    statPosjObj = StatistikaPosjecenosti.objects.all()
    statistikaReprodukcija = StatistikaReprodukcije.objects.all()
    pocetna = StatistikaPosjecenosti.objects.get(idstranica=0)

    ukupno = 0

    for stat in statPosjObj:
        ukupno += stat.brojprikaza

    izlosci = Izlozak.objects.all().order_by('nazivizlozak')

    for izlozak in izlosci:
        sum = 0
        for user in User.objects.all():
            for statistikaRedak in statistikaReprodukcija:
                if izlozak.idzvucnizapis == statistikaRedak.idzvucnizapis and user.id == statistikaRedak.korisnik_id:
                    sum += 1
                    break
        izlozak.idzvucnizapis.brojkorisnika = sum

    statPosj = []
    for s in statPosjObj:
        if s.idstranica != 0:
            tup = (s.stranicaizlozak_set.first().idizlozak.nazivizlozak,
                   s.brojprikaza,
                   "%d sati i %d minuta" % (s.vrijemezadrzavanja.seconds//3600, (s.vrijemezadrzavanja.seconds//60) % 60))
            statPosj.append(tup)

    statPosj = sorted(statPosj)
    sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
    grupeSPodgrupama = set()
    for grupa in sveGrupe:
        if grupa.idgrupa == grupa.idnadgrupa:
            for podgrupa in sveGrupe:
                if podgrupa.idnadgrupa == grupa.idgrupa and podgrupa.idgrupa != podgrupa.idnadgrupa:
                    grupeSPodgrupama.add(grupa.idgrupa)
                    break

    active_sessions = Session.objects.filter(expire_date__gte=timezone.now())
    user_id_list = []
    for session in active_sessions:
        data = session.get_decoded()
        user_id_list.append(data.get('_auth_user_id', None))
    # Query all logged in users based on id list
    prijavljeni_korisnici = User.objects.filter(id__in=user_id_list).count()

    context = {
        'pocetna': pocetna,
        'strIzlozak': strIzlozak,
        'statPosj': statPosj,
        'ukupno': ukupno,
        'izlosci': izlosci,
        'sveGrupe': sveGrupe,
        'grupeSPodgrupama': grupeSPodgrupama,
        'add_admin_form': AddAdminForm(),
        'admin_ovlast_form': AdminOvlastForm(),
        'izlozak_form': DodajIzlozakForm(),
        'prijavljeni_korisnici': prijavljeni_korisnici
    }
    return render(request, 'statistika/statistika.html', context)


def statistika_sort(request, param):
    strIzlozak = StranicaIzlozak.objects.all()
    statPosjObj = StatistikaPosjecenosti.objects.all()
    statistikaReprodukcija = StatistikaReprodukcije.objects.all()
    pocetna = StatistikaPosjecenosti.objects.get(idstranica=0)

    ukupno = 0

    for stat in statPosjObj:
        ukupno += stat.brojprikaza

    izlosci = Izlozak.objects.all()
    if param == 'reprodukcija_stranica':
        izlosci = sorted(izlosci, key=attrgetter('nazivizlozak'))
    elif param == 'reprodukcija_korisnik':
        zvucni_zapisi = ZvucniZapis.objects.all().order_by('brojkorisnika')
        izlosci = []
        for zapis in zvucni_zapisi:
            izlosci.append(Izlozak.objects.get(idzvucnizapis=zapis.idzvucnizapis))
        izlosci.reverse()
    elif param == 'reprodukcija_ukupno':
        zvucni_zapisi = ZvucniZapis.objects.all().order_by('brojreprodukcija').reverse()
        izlosci = []
        for zapis in zvucni_zapisi:
            izlozak = Izlozak.objects.get(idzvucnizapis=zapis.idzvucnizapis)
            if izlozak is not None:
                izlosci.append(izlozak)

    for izlozak in izlosci:
        sum = 0
        for user in User.objects.all():
            for statistikaRedak in statistikaReprodukcija:
                if izlozak.idzvucnizapis == statistikaRedak.idzvucnizapis and user.id == statistikaRedak.korisnik_id:
                    sum += 1
                    break
        izlozak.idzvucnizapis.brojkorisnika = sum

    statPosj = []
    for s in statPosjObj:
        if s.idstranica != 0:
            tup = (s.stranicaizlozak_set.first().idizlozak.nazivizlozak,
                   s.brojprikaza,
                   "%d sati i %d minuta" % (s.vrijemezadrzavanja.seconds//3600, (s.vrijemezadrzavanja.seconds//60) % 60))
            statPosj.append(tup)

    if param == 'posjecenost_stranica':
        statPosj = sorted(statPosj, key=itemgetter(0))
    elif param == 'posjecenost_prikaz':
        statPosj = sorted(statPosj, key=itemgetter(1), reverse=True)
    elif param == 'posjecenost_vrijeme':
        statPosj = sorted(statPosj, key=itemgetter(2), reverse=True)

    sveGrupe = GrupaIzlozaka.objects.all().order_by('nazivgrupa')
    grupeSPodgrupama = set()
    for grupa in sveGrupe:
        if grupa.idgrupa == grupa.idnadgrupa:
            for podgrupa in sveGrupe:
                if podgrupa.idnadgrupa == grupa.idgrupa and podgrupa.idgrupa != podgrupa.idnadgrupa:
                    grupeSPodgrupama.add(grupa.idgrupa)
                    break

    active_sessions = Session.objects.filter(expire_date__gte=timezone.now())
    user_id_list = []
    for session in active_sessions:
        data = session.get_decoded()
        user_id_list.append(data.get('_auth_user_id', None))
    # Query all logged in users based on id list
    prijavljeni_korisnici = User.objects.filter(id__in=user_id_list).count()

    context = {
        'pocetna': pocetna,
        'strIzlozak': strIzlozak,
        'statPosj': statPosj,
        'ukupno': ukupno,
        'izlosci': izlosci,
        'sveGrupe': sveGrupe,
        'grupeSPodgrupama': grupeSPodgrupama,
        'add_admin_form': AddAdminForm(),
        'admin_ovlast_form': AdminOvlastForm(),
        'izlozak_form': DodajIzlozakForm(),
        'prijavljeni_korisnici': prijavljeni_korisnici
    }
    return render(request, 'statistika/statistika.html', context)