from django.urls import path, re_path
import android.views

app_name = 'android'

urlpatterns = [
    re_path('izlozak(?P<izlozak_id>[0-9]+)/naslov', android.views.android_naslov, name='android_naslov'),
    re_path('izlozak(?P<izlozak_id>[0-9]+)/text', android.views.android_text, name='android_text'),
    re_path('izlozak(?P<izlozak_id>[0-9]+)/audio', android.views.android_audio, name='android_audio'),
    re_path('izlozak(?P<izlozak_id>[0-9]+)/slika', android.views.android_slika, name='android_slika'),
    path('registracija/', android.views.register, name='registracija'),
    path('prijava/', android.views.prijava, name='login'),

    path('statistika/<unique_id>/<izlozak_id>', android.views.android_statistika),
    path('povezi/<unique_id>', android.views.povezi),
    path('odvezi/<unique_id>', android.views.izbrisi_povezanost),
    path('provjeri/<unique_id>', android.views.jel_povezan),
]

