from django.shortcuts import render
from django.http import HttpResponse
from izlosci.models import Izlozak, ZvucniZapis, Slika, Aplikacija, StatistikaReprodukcije
from izlosci.forms import RegistrationForm, LoginForm
import izlosci.views


def android_naslov(request, izlozak_id):
    izlozak = Izlozak.objects.get(pk=izlozak_id)
    return HttpResponse(str(izlozak.nazivizlozak))


def android_text(request, izlozak_id):
    izlozak = Izlozak.objects.get(pk=izlozak_id)
    return HttpResponse(str(izlozak.opis))


def android_audio(request, izlozak_id):
    izlozak = Izlozak.objects.get(pk=izlozak_id)
    return HttpResponse("http://35.189.113.190:8000/" + str(izlozak.idzvucnizapis.zvucnizapis))


def android_slika(request, izlozak_id):
    izlozak = Izlozak.objects.get(pk=izlozak_id)
    return HttpResponse("http://35.189.113.190:8000/" + str(izlozak.idslika.slika))


def register(request):
    return izlosci.views.register(request)


def prijava(request):
    return izlosci.views.prijava(request)


def povezi(request, unique_id):
    if request.user.is_authenticated:  # ako je korisnik prijavljen onda samo povezi
        nova_aplikacija = Aplikacija(
            aplikacijaid=unique_id,
            korisnik=request.user,
        )
        nova_aplikacija.save()
        return HttpResponse("Povezivanje Uspjesno! Sada se mozete vratiti u aplikaciju")
    else:  # ako korisnik nije prijavljen ga vodi da se prijavi ili registrira
        request.session["unique_id"] = unique_id
        context = {
            'register_form': RegistrationForm(),
            'login_form': LoginForm(),
        }

        return render(request, 'androidLogin.html', context)


def jel_povezan(request, unique_id):
    apps = Aplikacija.objects.filter(aplikacijaid=unique_id)
    if not apps:
        return HttpResponse("nije")
    else:
        return HttpResponse("je")


def izbrisi_povezanost(request, unique_id):
    app = Aplikacija.objects.filter(aplikacijaid=unique_id)
    if app:
        app.delete()
        if request.user.is_authenticated:
            request.user.logout()
        return HttpResponse("Uspjesno")
    return HttpResponse("Neuspjesno")


def android_statistika(request, unique_id, izlozak_id):
    # TODO: integrirati ovo sa android_audio
    obj_zvucnizapis = ZvucniZapis.objects.get(izlozak=izlozak_id)
    try:
        obj_korisnik = (Aplikacija.objects.get(aplikacijaid=unique_id)).korisnik
    except Aplikacija.DoesNotExist:
        obj_korisnik = None

    playback = StatistikaReprodukcije(
        korisnik=obj_korisnik,
        idzvucnizapis=obj_zvucnizapis,
    )
    playback.save()
    obj_zvucnizapis.brojreprodukcija += 1
    obj_zvucnizapis.save()
    return HttpResponse("Uspjesno")

    # code unreachable!?
    # apps = Aplikacija.objects.filter(aplikacijaid=unique_id)
    # if not apps:
    #     return HttpResponse("nije")
    # else:
    #     return HttpResponse("je")



