package com.example.josip.myapplication;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.media.AudioManager;
import android.media.Image;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.Result;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.UUID;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class MainActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView scannerView;
    private MediaPlayer audioPlayer = new MediaPlayer();
    private ExhibitResource exhibitData;

    private String QR_URL;
    private String currentContent;
    private String UNIQUE_ANDROID_ID;

    private volatile boolean isUserLoggedIn;

    private int resolutionOfSeekBar = 10000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.initializeMainScreen();
        this.UNIQUE_ANDROID_ID = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID) + Build.SERIAL;

        showToast(this.UNIQUE_ANDROID_ID);

        this.isUserLoggedIn();

        this.scannerView = new ZXingScannerView(getApplicationContext());
        this.exhibitData = null;
    }

    @Override
    public void onPause() {
        super.onPause();

        if (this.audioPlayer.isPlaying()) {
            this.pauseAudio();
        }

        if (this.scannerView != null) {
            this.scannerView.stopCamera();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (this.currentContent.equals("main") || this.currentContent.equals("scan")
                || this.currentContent.equals("logged") || this.currentContent.equals("notLogged")) {
            this.turnOffCamera();

            if (this.isUserLoggedIn) {
                this.showLoggedInUserScreen();
            } else {
                this.showNotLoggedInUserScreen();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (this.currentContent.equals("main") || this.currentContent.equals("notLogged") || this.currentContent.equals("logged")) {
            super.onBackPressed();
        } else {
            this.turnOffCamera();

            if (this.isUserLoggedIn) {
                this.showLoggedInUserScreen();
            } else {
                this.showNotLoggedInUserScreen();
            }

            this.setTitle(R.string.app_name);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        audioPlayer.release();
    }

    @Override
    public void handleResult(Result result) {
        this.QR_URL = result.getText();
        this.turnOffCamera();

        this.showBeginingScreen();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    bufferingMode();

                    String audioLocation = QR_URL + "android/audio";
                    String exhibitDescriptionLocation = QR_URL + "android/text";
                    String titleDescription = QR_URL + "android/naslov";
                    String pictureLocation = QR_URL + "android/slika";

                    final String audioURL = getTextFromLocation(audioLocation, "Greška pri dohvaćanju audio lokacije!");
                    final String exhibitDescription = getTextFromLocation(exhibitDescriptionLocation, "Greška pri dohvaćanju opisa izloška!");
                    String titleBuffer = getTextFromLocation(titleDescription, "Greška pri učitavanju izloška!");
                    final String imageURL = getTextFromLocation(pictureLocation, "Greška pri dohvaćanju lokacije slike!");
                    final String title = titleBuffer.substring(0, 1).toUpperCase() + titleBuffer.substring(1, titleBuffer.length());

                    audioPlayer.reset();
                    audioPlayer.setLooping(false);
                    audioPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    audioPlayer.setDataSource("http://" + audioURL);

                    Bitmap image = loadImage("http://" + imageURL, "Greška pri dohvaćanju slike!");
                    audioPlayer.prepare();

                    exhibitData = new ExhibitResource(title, exhibitDescription, image);
                    showExhibit(exhibitData);

                } catch (IOException exception) {
                    showToast("IOE");
                    exception.printStackTrace();
                } catch (Exception exception) {
                    showToast("E");
                    exception.printStackTrace();
                }
            }
        }).start();
    }

    public void scan(View view) {
        this.setContentView(scannerView);
        this.currentContent = "scan";
        this.scannerView.setResultHandler(this);

        this.askForCameraPermission();

        this.scannerView.startCamera();
    }

    public void pausePlay() {
        if (audioPlayer.isPlaying()) {
            this.pauseAudio();
        } else {
            this.playAudio();
        }
    }

    protected void playAudio() {
        final ImageButton pausePlayButton = (ImageButton) findViewById(R.id.pause_play);
        final SeekBar bar = (SeekBar) findViewById(R.id.progressBar);

        pausePlayButton.setImageResource(R.drawable.pause);
        audioPlayer.start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (audioPlayer.isPlaying()) {
                    final int progress = (int) (((double) audioPlayer.getCurrentPosition() /
                            audioPlayer.getDuration()) *
                            resolutionOfSeekBar);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            bar.setProgress(progress);
                        }
                    });

                    try{
                        Thread.sleep(40);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    protected void pauseAudio() {
        ImageButton pausePlayButton = (ImageButton) findViewById(R.id.pause_play);
        pausePlayButton.setImageResource(R.drawable.play);
        audioPlayer.pause();
    }

    private void turnOffCamera() {
        this.scannerView.removeAllViews();
        this.scannerView.stopCamera();
    }

    private void initializeMainScreen() {
        setContentView(R.layout.activity_main);
        this.setAppColor(loadInt("color"));
        this.currentContent = "main";
    }

    private void bufferingMode() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //Button scanButton = (Button) findViewById(R.id.scanButton);
                //TextView bufferingText = (TextView) findViewById(R.id.bufferingText);

                //scanButton.setVisibility(View.INVISIBLE);
                //bufferingText.setVisibility(View.VISIBLE);
            }
        });
    }

    private void showToast(final String toast) {
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(MainActivity.this, toast, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void showExhibit(final ExhibitResource exhibitData) {
        runOnUiThread(new Runnable() {
            public void run() {
                setContentView(R.layout.exhibit_view);
                currentContent = "exhibit";
                setTitle(exhibitData.getExhibitTitle());

                TextView text = (TextView) findViewById(R.id.exhibit_test);
                text.setText(exhibitData.getExhibitDescription());

                ImageView exhibitImage = (ImageView) findViewById(R.id.exhibit_image);
                exhibitImage.setImageBitmap(exhibitData.getExhibitImage());
                exhibitImage.setMaxHeight(exhibitData.getExhibitImage().getHeight());

                SeekBar audioProgressBar = (SeekBar) findViewById(R.id.progressBar);
                audioProgressBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if(fromUser) {
                            audioPlayer.seekTo((int)((double)progress / resolutionOfSeekBar * audioPlayer.getDuration()));
                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        audioPlayer.pause();
                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        playAudio();
                    }
                });

                ImageButton pausePlayButton = (ImageButton) findViewById(R.id.pause_play);
                pausePlayButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pausePlay();
                    }
                });

                audioPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer player) {
                        ImageButton btn = (ImageButton) findViewById(R.id.pause_play);
                        btn.setImageResource(R.drawable.play);
                        SeekBar bar = (SeekBar) findViewById(R.id.progressBar);
                        bar.setProgress(0);
                    }
                });
            }
        });
    }

    private String getTextFromLocation(final String location, String errorMessage) {

        String text = null;

        try {
            StringBuilder builder = new StringBuilder();
            URL url = new URL(location);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(10000);

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            String str;
            while ((str = in.readLine()) != null) {
                builder.append(str);
            }

            text = builder.toString();
            conn.disconnect();
            in.close();
        } catch (Exception e) {
            showToast(errorMessage);
        }

        return text;
    }

    private Bitmap loadImage(String location, String errorMessage) {
        try {
            URL url = new URL(location);
            return BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (Exception e) {
            showToast(errorMessage);
        }

        return null;
    }

    private void askForCameraPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA}, 0);
        }
    }

    private void sendQrScanedOnServer(String URL) {
        String[] buff = URL.split("/");
        String exhibitID = buff[buff.length - 1];
        exhibitID = exhibitID.replace("izlozak", "");

        //showToast(exhibitID);
        //showToast(UUID.randomUUID().toString());
        try {
            URL url = new URL("http://192.168.43.33:8000/izlosci/android/statistika");
            String tmp2 = "http://192.168.1.8:8000/izlosci/android/statistika";

            //HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            String tmp = getTextFromLocation(tmp2+ "/" + UUID.randomUUID().toString() + "/" + exhibitID, "NE RADI!");
            //urlConnection.setReadTimeout(10000);
            //urlConnection.setConnectTimeout(10000);
            //urlConnection.setDoInput(true);
            //urlConnection.setDoOutput(true);


            //urlConnection.connect();
            /*try {
                Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(tmp2));
                startActivity(myIntent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(this, "No application can handle this request."
                        + " Please install a webbrowser",  Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }*/


            showToast(tmp);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            showToast("U CATCHU!");
            e.printStackTrace();
        }
        //getTextFromLocation("http://192.168.43.29:8000/izlosci/android/statistika", "NIJE USPJELO!");
    }

    public void colorSelected(View view) {
        ImageButton btn = (ImageButton) view;

        switch (btn.getId()){
            case R.id.redButton:
                setAppColor(R.color.red);
                saveInt("color", R.color.red);
                break;
            case R.id.redBrightButton:
                setAppColor(R.color.redBright);
                saveInt("color", R.color.redBright);
                break;
            case R.id.brownButton:
                setAppColor(R.color.brown);
                saveInt("color", R.color.brown);
                break;
            case R.id.greenButton:
                setAppColor(R.color.green);
                saveInt("color", R.color.green);
                break;
            case R.id.yellowButton:
                setAppColor(R.color.yellow);
                saveInt("color", R.color.yellow);
                break;
            case R.id.cyanButton:
                setAppColor(R.color.cyan);
                saveInt("color", R.color.cyan);
                break;
            case R.id.purpleButton:
                setAppColor(R.color.purple);
                saveInt("color", R.color.purple);
                break;
            case R.id.orangeButton:
                setAppColor(R.color.orange);
                saveInt("color", R.color.orange);
                break;
            case R.id.blueButton:
                setAppColor(R.color.blue);
                saveInt("color", R.color.blue);
                break;
        }
    }

    public void showColorOptionsScreen(View view) {
        setContentView(R.layout.color_options);
        currentContent = "colors";
    }

    private void showLoggedInUserScreen() {
        setContentView(R.layout.logged_in_view);
        currentContent = "notLogged";
    }

    private void showNotLoggedInUserScreen() {
        setContentView(R.layout.not_logged_in_view);
        currentContent = "logged";
    }

    private void showPromoMaterials() {
        try {
            Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(""));
            startActivity(myIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "No application can handle this request."
                    + " Please install a webbrowser",  Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    private void isUserLoggedIn() {

        if (!this.isNetworkAvailable()) {
            this.isUserLoggedIn = false;
        } else {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    final String loginTestSite = "";

                    try {
                        URL url = new URL(loginTestSite);

                        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                        conn.setConnectTimeout(1500);

                        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                        String str = in.readLine();

                        if (str.equals("je")) {
                            isUserLoggedIn = true;
                        } else {
                            isUserLoggedIn = false;
                        }

                        showBeginingScreen();
                    } catch (MalformedURLException exception) {
                        isUserLoggedIn = false;
                    } catch (IOException exception) {
                        isUserLoggedIn = false;
                    }
                }
            }).start();
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void showBeginingScreen() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isUserLoggedIn) {
                    setContentView(R.layout.logged_in_view);
                    currentContent = "logged";
                } else {
                    setContentView(R.layout.logged_in_view);
                    currentContent = "notLogged";
                }

                setTitle(R.string.app_name);
            }
        });
    }

    private void setAppColor(int color) {
        if (color != -1) {
            this.getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                    .getColor(color)));
        } else {
            this.getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(R.color.red)));
        }

        //SeekBar seekBar = (SeekBar) findViewById(R.id.progressBar);
        //LayerDrawable ld = (LayerDrawable) seekBar.getProgressDrawable();
        //ClipDrawable d1 = (ClipDrawable) ld.findDrawableByLayerId(R.id.progress);
        //d1.setColorFilter(color, PorterDuff.Mode.SRC_IN);

        /*LayerDrawable layerDrawable = (LayerDrawable) getResources()
                .getDrawable(R.drawable.progress_bar);
        GradientDrawable gradientDrawable = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.progress);
        gradientDrawable.setColor(color); // change color*/
    }

    public void saveInt(String key, int value){
        SharedPreferences preferences = getSharedPreferences("AppPreferences", this.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public int loadInt(String key){
        SharedPreferences settings = getSharedPreferences("AppPreferences", 0);
        return settings.getInt(key, -1);
    }
}
