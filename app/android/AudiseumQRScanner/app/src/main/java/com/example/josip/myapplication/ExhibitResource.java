package com.example.josip.myapplication;

import android.graphics.Bitmap;

/**
 * Created by Josip Nimac on 14.1.2018..
 */

public class ExhibitResource {

    private String exhibitTitle;
    private String exhibitDescription;
    private Bitmap exhibitImage;

    public ExhibitResource(String title, String description, Bitmap image) {
        this.exhibitTitle = title;
        this.exhibitDescription = description;
        this.exhibitImage = image;
    }

    public String getExhibitTitle() {
        return exhibitTitle;
    }

    public void setExhibitTitle(String exhibitTitle) {
        this.exhibitTitle = exhibitTitle;
    }

    public String getExhibitDescription() {
        return exhibitDescription;
    }

    public void setExhibitDescription(String exhibitDescription) {
        this.exhibitDescription = exhibitDescription;
    }

    public Bitmap getExhibitImage() {
        return exhibitImage;
    }

    public void setExhibitImage(Bitmap exhibitImage) {
        this.exhibitImage = exhibitImage;
    }
}
